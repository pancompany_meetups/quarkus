package nl.cys.employee;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/employee")
public class EmployeeResource {

    @Inject
    EmployeeService employeeService;

    @GET
    @Path("/listall")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> listAll() {
        return employeeService.getAllEmployees();
    }

    @GET
    @Path("{lastname}")
    public Employee findByLastName(@PathParam("lastname") String lastname){
        return employeeService.findByLastName(lastname);
    }

    @POST
    @Path("/employ")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String employ(Employee employee) {
        return employeeService.employNewEmployee(employee);
    }
}
