package nl.cys.employee;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class EmployeeRepository implements PanacheRepository<Employee> {

    public Employee findByLastName(String lastname){
        return find("lastname", lastname).firstResult();
    }

    public List<Employee> findByFunction(String function) {
        return find("function", function).list();
    }
}
