package nl.cys.employee;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class EmployeeService {

    @Inject
    EmployeeRepository employeeRepository;

    public Employee findByLastName(String lastname) {
        return employeeRepository.findByLastName(lastname);
    }

    public List<Employee> getAllEmployees() {
        List<Employee> employees = employeeRepository.findAll().list();
        return employees;
    }

    public String employNewEmployee(Employee employee) {
        if (employee.getFunction().equals("TheBoss")) {
            throw new IllegalArgumentException("There can be only one");
        } else {
            employeeRepository.persist(employee);
            return employee.getIdStringValue();
        }

    }
}
