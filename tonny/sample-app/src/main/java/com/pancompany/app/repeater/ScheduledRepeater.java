package com.pancompany.app.repeater;

import io.quarkus.scheduler.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ScheduledRepeater {

    private static final Logger LOG = LoggerFactory.getLogger(ScheduledRepeater.class);

    @Scheduled(every = "PT5.0S")
    void printHeartBeat() {
        LOG.info("I'm Alive");
    }


}
