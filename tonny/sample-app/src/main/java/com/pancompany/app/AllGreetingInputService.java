package com.pancompany.app;

import com.pancompany.demo.library.GreetingInputService;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

@ApplicationScoped
@Alternative
public class AllGreetingInputService implements GreetingInputService {

    @Override
    public String getAudience() {
        return "All";
    }
}
