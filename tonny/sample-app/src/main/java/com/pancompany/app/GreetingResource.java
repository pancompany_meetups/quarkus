package com.pancompany.app;

import com.pancompany.demo.library.GreetingService;
import io.quarkus.security.identity.SecurityIdentity;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    private final GreetingService greetingConverter;

    GreetingResource(GreetingService greetingConverter) {
        this.greetingConverter = greetingConverter;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return greetingConverter.greet();
    }
}