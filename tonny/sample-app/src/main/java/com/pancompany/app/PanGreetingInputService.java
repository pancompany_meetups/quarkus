package com.pancompany.app;

import com.pancompany.demo.library.GreetingInputService;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PanGreetingInputService implements GreetingInputService {

    @Override
    public String getAudience() {
        return "Pan Colleagues";
    }
}
