package com.pancompany.demo.library;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class GreetingServiceTest {

    @Inject
    GreetingService greetingService;

    @Test
    void testGreetingService() {
        String result = greetingService.greet();
        assertEquals("HELLO Test Audience", result);
    }

    @Produces
    GreetingInputService getGreetingInputService() {
        return () -> "Test Audience";
    }
}
