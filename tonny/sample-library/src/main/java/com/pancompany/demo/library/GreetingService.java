package com.pancompany.demo.library;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingService {

    private final GreetingInputService greetingInputService;

    GreetingService(GreetingInputService greetingInputService) {
        this.greetingInputService = greetingInputService;
    }

    public String greet() {
        String greeting = greetingInputService.getAudience();
        return "HELLO ".concat(greeting != null ? greeting : "EVERYONE");
    }

}
