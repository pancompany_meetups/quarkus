package com.pancompany.demo.library;

@FunctionalInterface
public interface GreetingInputService {

    String getAudience();

}
